package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_blockquote extends HtmlContainer<HtmlWrapper> {

	public Html_blockquote() {
		super("blockquote");
	}

	public static final class CITE extends HtmlAttribute {
		public CITE(String value) {
			super("cite", value);
		}
	}
}
