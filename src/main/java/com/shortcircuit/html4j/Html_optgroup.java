package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_optgroup<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_optgroup() {
		super("optgroup");
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class LABEL extends HtmlAttribute {
		public LABEL(String value) {
			super("label", value);
		}
	}
}
