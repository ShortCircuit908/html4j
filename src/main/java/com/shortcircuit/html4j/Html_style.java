package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_style extends HtmlContainer<HtmlWrapper> {

	public Html_style() {
		super("style");
	}

	public static final class MEDIA extends HtmlAttribute {
		public MEDIA(String value) {
			super("media", value);
		}
	}

	public static final class SCOPED extends HtmlAttribute {
		public SCOPED() {
			super("scoped", null);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}
}
