package com.shortcircuit.html4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_time extends HtmlContainer<HtmlWrapper> {

	public Html_time() {
		super("time");
	}

	public static final class DATETIME extends HtmlAttribute {
		private static final SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DDThh:mm:ssTZD");

		public DATETIME(Date value) {
			super("datetime", format.format(value));
		}

		private DATETIME(String value) {
			super("datetime", value);
		}
	}
}
