package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_br extends HtmlWrapper {

	public Html_br() {
		super("br", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}
}
