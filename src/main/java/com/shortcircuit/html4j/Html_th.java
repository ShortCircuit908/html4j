package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_th<T extends HtmlWrapper> extends HtmlContainer<T> {

	public Html_th() {
		super("th");
	}

	public static final class ABBR extends HtmlAttribute {
		public ABBR(String value) {
			super("abbr", value);
		}
	}

	public static final class COLSPAN extends HtmlAttribute {
		public COLSPAN(Number value) {
			super("colspan", value == null ? "0" : value.toString());
		}
	}

	public static final class HEADERS extends HtmlAttribute {
		public HEADERS(String value) {
			super("headers", ' ', value);
		}
	}

	public static final class ROWSPAN extends HtmlAttribute {
		public ROWSPAN(Number value) {
			super("rowspan", value == null ? "0" : value.toString());
		}
	}

	public static final class SCOPE extends HtmlAttribute {
		public SCOPE(_Scope value) {
			super("scope", value.scope);
		}

		private SCOPE(String value) {
			super("scope", value);
		}
	}

	public static final class SORTED extends HtmlAttribute {
		public SORTED(boolean reversed, Number value) {
			super("sorted", "reversed " + (value == null ? "0" : value.toString()));
		}

		public SORTED(Number value, boolean reversed) {
			super("sorted", (value == null ? "0" : value.toString()) + " reversed");
		}

		public SORTED(boolean reversed) {
			super("sorted", "reversed");
		}

		public SORTED(Number value) {
			super("sorted", value == null ? "0" : value.toString());
		}

		private SORTED(String value) {
			super("sorted", value);
		}
	}

	public enum _Scope {
		COL("col"),
		COLGROUP("colgroup"),
		ROW("row"),
		ROWGROUP("rowgroup");

		private final String scope;

		_Scope(String scope) {
			this.scope = scope;
		}
	}
}
