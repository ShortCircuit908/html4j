package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_link extends HtmlWrapper {

	public Html_link() {
		super("link", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class CROSSORIGIN extends HtmlAttribute {
		public CROSSORIGIN(_Crossorigin value) {
			super("crossorigin", value.crossorigin);
		}

		private CROSSORIGIN(String value) {
			super("crossorigin", value);
		}
	}

	public static final class HREF extends HtmlAttribute {
		public HREF(String value) {
			super("href", value);
		}
	}

	public static final class HREFLANG extends HtmlAttribute {
		public HREFLANG(String value) {
			super("hreflang", value);
		}
	}

	public static final class MEDIA extends HtmlAttribute {
		public MEDIA(String value) {
			super("media", value);
		}
	}

	public static final class REL extends HtmlAttribute {
		public REL(_Rel value) {
			super("rel", value.rel);
		}

		public REL(String value) {
			super("rel", value);
		}
	}

	public static final class SIZES extends HtmlAttribute {
		public SIZES(_Sizes value) {
			super("sizes", value.sizes);
		}

		public SIZES(String value) {
			super("sizes", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}

	public enum _Crossorigin {
		ANONYMOUS("anonymous"),
		USE_CREDENTIALS("use-credentials");

		private final String crossorigin;

		_Crossorigin(String crossorigin) {
			this.crossorigin = crossorigin;
		}
	}

	public enum _Rel {
		ALTERNATE("alternate"),
		ARCHIVES("archives"),
		AUTHOR("author"),
		BOOKMARK("bookmark"),
		EXTERNAL("external"),
		FIRST("first"),
		HELP("help"),
		ICON("icon"),
		LAST("last"),
		LICENSE("license"),
		NEXT("next"),
		NOFOLLOW("nofollow"),
		NOREFERRER("noreferrer"),
		PINGBACK("pingback"),
		PREFETCH("prefetch"),
		PREV("prev"),
		SEARCH("search"),
		SIDEBAR("sidebar"),
		STYLESHEET("stylesheet"),
		TAG("tag"),
		UP("up");

		private final String rel;

		_Rel(String rel) {
			this.rel = rel;
		}
	}

	public enum _Sizes {
		ANY("any");

		private final String sizes;

		_Sizes(String sizes) {
			this.sizes = sizes;
		}
	}
}
