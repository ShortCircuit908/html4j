package com.shortcircuit.html4j;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_input extends HtmlContainer<HtmlWrapper> {

	public Html_input() {
		super("input");
	}

	public static final class ACCEPT extends HtmlAttribute {
		public ACCEPT(String value) {
			super("action", ',', value);
		}
	}

	public static final class ALT extends HtmlAttribute {
		public ALT(String value) {
			super("alt", value);
		}
	}

	public static final class AUTOCOMPLETE extends HtmlAttribute {
		public AUTOCOMPLETE(boolean value) {
			super("autocomplete", value ? "on" : "off");
		}
	}

	public static final class AUTOFOCUS extends HtmlAttribute {
		public AUTOFOCUS() {
			super("autofocus", null);
		}
	}

	public static final class CHECKED extends HtmlAttribute {
		public CHECKED() {
			super("checked", null);
		}
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class FORMACTION extends HtmlAttribute {
		public FORMACTION(String value) {
			super("formaction", value);
		}
	}

	public static final class FORMENCTYPE extends HtmlAttribute {
		public FORMENCTYPE(_FormEncType value) {
			super("formenctype", value.enctype);
		}

		private FORMENCTYPE(String value) {
			super("formenctype", value);
		}
	}

	public static final class FORMMETHOD extends HtmlAttribute {
		public FORMMETHOD(_FormMethod value) {
			super("formmethod", value.method);
		}

		private FORMMETHOD(String value) {
			super("formmethod", value);
		}
	}

	public static final class FORMNOVALIDATE extends HtmlAttribute {
		public FORMNOVALIDATE() {
			super("formnovalidate", null);
		}
	}

	public static final class FORMTARGET extends HtmlAttribute {
		public FORMTARGET(_FormTarget value) {
			this(value.target);
		}

		public FORMTARGET(String value) {
			super("formtarget", value);
		}
	}

	public static final class HEIGHT extends HtmlAttribute {
		public HEIGHT(Number value) {
			super("height", value == null ? "0" : value.toString());
		}
	}

	public static final class LIST extends HtmlAttribute {
		public LIST(String value) {
			super("list", value);
		}
	}

	public static final class MAX extends HtmlAttribute {
		private static final SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD");

		public MAX(Number value) {
			super("max", value == null ? "0" : value.toString());
		}

		public MAX(Date value) {
			super("max", format.format(value));
		}

		private MAX(String value) {
			super("max", value);
		}
	}

	public static final class MAXLENGTH extends HtmlAttribute {
		public MAXLENGTH(Number value) {
			super("maxlength", value == null ? "0" : value.toString());
		}
	}

	public static final class MIN extends HtmlAttribute {
		private static final SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD");

		public MIN(Number value) {
			super("min", value == null ? "0" : value.toString());
		}

		public MIN(Date value) {
			super("min", format.format(value));
		}

		private MIN(String value) {
			super("min", value);
		}
	}

	public static final class MULTIPLE extends HtmlAttribute {
		public MULTIPLE() {
			super("multiple", null);
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class PATTERN extends HtmlAttribute {
		public PATTERN(Pattern value) {
			super("pattern", value.pattern());
		}

		public PATTERN(String value) throws PatternSyntaxException {
			super("pattern", Pattern.compile(value).pattern());
		}
	}

	public static final class PLACEHOLDER extends HtmlAttribute {
		public PLACEHOLDER(String value) {
			super("placeholder", value);
		}
	}

	public static final class READONLY extends HtmlAttribute {
		public READONLY() {
			super("readonly", null);
		}
	}

	public static final class REQUIRED extends HtmlAttribute {
		public REQUIRED() {
			super("required", null);
		}
	}

	public static final class SIZE extends HtmlAttribute {
		public SIZE(Number value) {
			super("size", value == null ? "0" : value.toString());
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class STEP extends HtmlAttribute {
		public STEP(Number value) {
			super("step", value == null ? "0" : value.toString());
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(_Type value) {
			super("type", value.type);
		}

		private TYPE(String value){
			super("type", value);
		}
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(String value) {
			super("value", value);
		}
	}

	public static final class WIDTH extends HtmlAttribute {
		public WIDTH(Number value) {
			super("width", value == null ? "0" : value.toString());
		}
	}

	public enum _FormTarget {
		BLANK("_blank"),
		PARENT("_parent"),
		SELF("_self"),
		TOP("_top");
		private final String target;

		_FormTarget(String target) {
			this.target = target;
		}
	}

	public enum _FormEncType {
		APPLICATION("application/x-www-form-urlencoded"),
		MULTIPART("multipart/form-data"),
		PLAIN_TEXT("text/plain");

		private final String enctype;

		_FormEncType(String enctype) {
			this.enctype = enctype;
		}
	}

	public enum _FormMethod {
		GET("get"),
		POST("post");

		private final String method;

		_FormMethod(String method) {
			this.method = method;
		}
	}

	public enum _Type {
		BUTTON("button"),
		CHECKBOX("checkbox"),
		COLOR("color"),
		DATE("date"),
		DATETIME("datetime"),
		DATETIME_LOCAL("datetime-local"),
		EMAIL("email"),
		FILE("file"),
		HIDDEN("hidden"),
		IMAGE("image"),
		MONTH("month"),
		NUMBER("number"),
		PASSWORD("password"),
		RADIO("radio"),
		RANGE("range"),
		RESET("reset"),
		SEARCH("search"),
		SUBMIT("submit"),
		TEL("tel"),
		TEXT("text"),
		TIME("time"),
		URL("url"),
		WEEL("week");

		private final String type;

		_Type(String type) {
			this.type = type;
		}
	}

}
