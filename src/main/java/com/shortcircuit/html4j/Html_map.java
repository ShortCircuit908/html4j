package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_map<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_map() {
		super("map");
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}
}
