package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/8/2015
 */
public class Html_li<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_li() {
		super("li");
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(Number value) {
			super("value", value == null ? "0" : value.toString());
		}
	}
}
