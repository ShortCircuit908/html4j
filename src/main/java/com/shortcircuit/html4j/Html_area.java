package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_area extends HtmlWrapper {

	public Html_area() {
		super("area", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class ALT extends HtmlAttribute {
		public ALT(String value) {
			super("alt", value);
		}
	}

	public static final class COORDS extends HtmlAttribute {
		public COORDS(String value) {
			super("coords", value);
		}
	}

	public static final class DOWNLOAD extends HtmlAttribute {
		public DOWNLOAD(String value) {
			super("downloadHtmlA", value);
		}
	}

	public static final class HREF extends HtmlAttribute {
		public HREF(String value) {
			super("href", value);
		}
	}

	public static final class HREFLANG extends HtmlAttribute {
		public HREFLANG(String value) {
			super("hreflang", value);
		}
	}

	public static final class MEDIA extends HtmlAttribute {
		public MEDIA(String value) {
			super("media", value);
		}
	}

	public static final class REL extends HtmlAttribute {
		public REL(_Rel value) {
			this(value.rel);
		}

		public REL(String value) {
			super("rel", value);
		}
	}

	public static final class SHAPE extends HtmlAttribute {
		public SHAPE(_Shape value) {
			super("shape", value.shape);
		}

		private SHAPE(String value){
			super("shape", value);
		}
	}

	public static final class TARGET extends HtmlAttribute {
		public TARGET(_Target value) {
			super("target", value.target);
		}

		public TARGET(String value) {
			super("target", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}

	public enum _Rel {
		ALTERNATE("alternate"),
		AUTHOR("author"),
		BOOKMARK("bookmark"),
		HELP("help"),
		LICENSE("license"),
		NEXT("next"),
		NOFOLLOW("nofollow"),
		NOREFERRER("noreferrer"),
		PREFETCH("prefetch"),
		PREV("prev"),
		SEARCH("search"),
		TAG("tag");

		private final String rel;

		_Rel(String rel) {
			this.rel = rel;
		}
	}

	public enum _Shape {
		DEFAULT("default"),
		RECT("rect"),
		CIRCLE("circle"),
		POLY("poly");

		private final String shape;

		_Shape(String shape) {
			this.shape = shape;
		}
	}

	public enum _Target {
		BLANK("_blank"),
		PARENT("_parent"),
		SELF("_self"),
		TOP("_top");
		private final String target;

		_Target(String target) {
			this.target = target;
		}
	}
}
