package com.shortcircuit.html4j.parse;

import com.shortcircuit.html4j.*;

import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 10/12/2015
 */
public class Parser {
	private static final String TAG_PATTERN = "</?\\w+((\\s+(\\w\\-?)*+(\\s*=\\s*(?:\".*?\"|'.*?'|[^'\">\\s]+))?)+\\s*|\\s*)/?>";
	private static final String COMMENT_PATTERN = "<!\\-\\-|\\-\\->";
	private static final String ATTRIBUTE_PATTERN = "(\\w\\-?)*+(\\s*=\\s*(?:\".*?\"|'.*?'|[^'\">\\s]))";
	private static final String DOCTYPE_PATTERN = "<!\\s*DOCTYPE.*?>";

	public static HtmlWrapper parse(String document) {
		LinkedList<Tag> tags = splitTags(stripDoctype(document)[0]);
		EmptyContainer empty = new EmptyContainer();
		try {
			parseTags(empty, tags);
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		return empty.container;
	}

	private static String[] stripDoctype(String document) {
		document = document.trim();
		int index;
		String sub;
		for (index = 0; index < document.length(); index++) {
			sub = document.substring(0, index);
			if (index > 1 && sub.endsWith("<")) {
				break;
			}
			if (sub.matches(DOCTYPE_PATTERN)) {
				return new String[]{document.substring(index, document.length()), sub};
			}
		}
		return new String[]{document};
	}

	public static String formatHtml(String document) {
		return formatHtml(document, false, 4);
	}

	public static String formatHtml(String document, int space_indent) {
		return formatHtml(document, false, space_indent);
	}

	public static String formatHtml(String document, boolean use_tabs) {
		return formatHtml(document, use_tabs, 4);
	}

	public static String formatHtml(String document, boolean use_tabs, int space_indent) {
		StringBuilder builder = new StringBuilder();
		String[] split = stripDoctype(document.trim());
		document = split[0];
		if (split.length > 1) {
			builder.append(split[1]);
		}
		int indent_level = 0;
		LinkedList<Tag> tags = splitTags(document);
		String val;
		for (Tag tag : tags) {
			if (tag.isClosingTag()) {
				indent_level--;
			}
			if (use_tabs) {
				for (int i = 0; i < indent_level; i++) {
					builder.append('\t');
				}
			}
			else {
				for (int i = 0; i < indent_level * space_indent; i++) {
					builder.append(' ');
				}
			}
			val = tag.getValue();
			if (!tag.isRawString() && !tag.isComment()) {
				val = '<' + val + ">";
			}
			builder.append(val).append('\n');
			if (!tag.isClosingTag() && !tag.isRawString() && !tag.isSelfClosing()) {
				indent_level++;
			}
		}
		return builder.toString();
	}

	private static LinkedList<Tag> splitTags(String document) {
		document = stripDoctype(document.trim())[0];
		LinkedList<Tag> tags = new LinkedList<>();
		int index;
		int last_index = 0;
		boolean raw_string;
		String sub;
		for (index = last_index; index <= document.length(); index++) {
			sub = document.substring(last_index, index);
			raw_string = (!sub.startsWith("<") && !sub.endsWith("-->"));
			if (raw_string) {
				if (document.charAt(index) == '<') {
					last_index = index;
					tags.add(new Tag(sub, true));
				}
			}
			else if (sub.matches(TAG_PATTERN) || sub.matches(COMMENT_PATTERN)) {
				last_index = index;
				tags.add(new Tag(sub, false));
			}
		}
		return tags;
	}

	private static LinkedList<String> splitAttributes(String attribute_str) {
		LinkedList<String> attributes = new LinkedList<>();
		int index;
		int last_index = 0;
		String sub;
		for (index = last_index; index <= attribute_str.length(); index++) {
			sub = attribute_str.substring(last_index, index).trim();
			if (sub.matches(ATTRIBUTE_PATTERN)) {
				last_index = index;
				attributes.add(sub);
			}
		}
		return attributes;
	}

	private static HtmlContainer parseTags(HtmlContainer parent, LinkedList<Tag> tags) throws ReflectiveOperationException {
		Tag tag;
		String[] parts;
		Class<? extends HtmlWrapper> element_class;
		HtmlWrapper element;
		String[] attribute_parts, data_parts;
		Class<? extends HtmlAttribute> attribute_class;
		String value, data_type, classname;
		HtmlAttribute attribute;
		HtmlString string;
		while (tags.size() > 0) {
			tag = tags.pop();
			if (!tag.isRawString()) {
				if (!tag.isClosingTag()) {
					parts = tag.getValue().split(" ", 2);
					if (tag.isComment()) {
						element_class = HtmlComment.class;
					}
					else {
						element_class = (Class<? extends HtmlWrapper>) Class.forName("com.shortcircuit.html4j.Html_" + parts[0]);
					}
					element = element_class.newInstance();
					if (element instanceof HtmlContainer) {
						parent.add(parseTags((HtmlContainer) element, tags));
					}
					else {
						parent.add(element);
					}
					if (parts.length > 1) {
						for (String attribute_str : splitAttributes(parts[1])) {
							attribute_parts = attribute_str.split("=", 2);
							value = attribute_parts.length > 1 ? attribute_parts[1] : "";
							if (value != null) {
								value = value.substring(1, value.length() - 1);
							}
							if (attribute_parts[0].startsWith("data-")) {
								data_parts = attribute_parts[0].split("\\-", 2);
								data_type = data_parts.length > 1 ? data_parts[1] : "";
								attribute = new HtmlWrapper.DATA(data_type, value);
							}
							else {
								try {
									classname = element.getClass().getCanonicalName() + "$" + attribute_parts[0].replace('-', '_').toUpperCase();
									attribute_class = (Class<? extends HtmlAttribute>) Class.forName(classname);
								}
								catch (ClassNotFoundException e) {
									classname = "com.shortcircuit.html4j.HtmlWrapper$" + attribute_parts[0].replace('-', '_').toUpperCase();
									attribute_class = (Class<? extends HtmlAttribute>) Class.forName(classname);
								}
								attribute = HtmlAttribute.newAttribute(attribute_class, value);
							}
							element.setAttribute(attribute);
						}
					}
				}
				else {
					return parent;
				}
			}
			else {
				string = new HtmlString();
				string.add(tag.getValue());
				parent.add(string);
			}
		}
		return parent;
	}

	private static class Tag {
		private final String value;
		private final boolean raw_string;

		public Tag(String value, boolean raw_string) {
			this.raw_string = raw_string;
			if (!raw_string && !value.matches(COMMENT_PATTERN)) {
				value = value.substring(1, value.length() - 1);
			}
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public boolean isRawString() {
			return raw_string;
		}

		public boolean isSelfClosing() {
			return !raw_string && value.endsWith("/");
		}

		public boolean isClosingTag() {
			return !raw_string && (value.startsWith("/") || value.endsWith("-->"));
		}

		public boolean isComment() {
			return !raw_string && (value.startsWith("<!--") || value.endsWith("-->"));
		}
	}

	private static class EmptyContainer extends HtmlContainer {
		private HtmlWrapper container = null;

		public EmptyContainer() {
			super("EMPTY");
		}

		@Override
		public boolean add(HtmlWrapper wrapper) {
			container = wrapper;
			return true;
		}

		@Override
		public String toHtmlString() {
			return container == null ? "" : container.toHtmlString();
		}
	}
}
