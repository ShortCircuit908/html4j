package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_video<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_video() {
		super("video");
	}

	public static final class AUTOPLAY extends HtmlAttribute {
		public AUTOPLAY() {
			super("autoplay", null);
		}
	}

	public static final class HEIGHT extends HtmlAttribute {
		public HEIGHT(Number value) {
			super("height", value == null ? "0" : value.toString());
		}
	}

	public static final class CONTROLS extends HtmlAttribute {
		public CONTROLS() {
			super("controls", null);
		}
	}

	public static final class LOOP extends HtmlAttribute {
		public LOOP() {
			super("loop", null);
		}
	}

	public static final class MUTED extends HtmlAttribute {
		public MUTED() {
			super("muted", null);
		}
	}

	public static final class POSTER extends HtmlAttribute {
		public POSTER(String value) {
			super("poster", value);
		}
	}

	public static final class PRELOAD extends HtmlAttribute {
		public PRELOAD() {
			super("preload", null);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class WIDTH extends HtmlAttribute {
		public WIDTH(Number value) {
			super("width", value == null ? "0" : value.toString());
		}
	}
}
