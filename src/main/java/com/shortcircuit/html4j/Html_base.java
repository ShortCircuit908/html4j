package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_base extends HtmlContainer<HtmlWrapper> {

	public Html_base() {
		super("base");
	}

	public static final class HREF extends HtmlAttribute {
		public HREF(String value) {
			super("href", value);
		}
	}

	public static final class TARGET extends HtmlAttribute {
		public TARGET(_Target value) {
			super("target", value.target);
		}

		public TARGET(String value) {
			super("target", value);
		}
	}

	public enum _Target {
		BLANK("_blank"),
		PARENT("_parent"),
		SELF("_self"),
		TOP("_top");
		private final String target;

		_Target(String target) {
			this.target = target;
		}
	}
}
