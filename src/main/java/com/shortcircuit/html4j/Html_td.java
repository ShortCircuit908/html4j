package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_td<T extends HtmlWrapper> extends HtmlContainer<T> {

	public Html_td() {
		super("td");
	}

	public static final class COLSPAN extends HtmlAttribute {
		public COLSPAN(Number value) {
			super("colspan", value == null ? "0" : value.toString());
		}
	}

	public static final class HEADERS extends HtmlAttribute {
		public HEADERS(String value) {
			super("headers", ' ', value);
		}
	}

	public static final class ROWSPAN extends HtmlAttribute {
		public ROWSPAN(Number value) {
			super("rowspan", value == null ? "0" : value.toString());
		}
	}
}
