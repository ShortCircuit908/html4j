package com.shortcircuit.html4j;

import java.lang.reflect.Constructor;

/**
 * Represents an HTML attribute which may be associated with a given element
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class HtmlAttribute {
	private final String name;
	private final char delimiter;
	private String value;

	/**
	 * Construct a new attribute with the given name and value, specifying the default delimiter {@code ;}
	 * <p>
	 * If {@code value} is {@code null}, the attribute is regarded as having no value.
	 * </p>
	 *
	 * @param name  the name of the attribute
	 * @param value the value of the attribute
	 */
	public HtmlAttribute(String name, String value) {
		this(name, ';', value);
	}

	/**
	 * Construct a new attribute with the given name and value, while specifying a delimiter to use while appending values
	 * <p>
	 * If {@code value} is {@code null}, the attribute is regarded as having no value.
	 * </p>
	 *
	 * @param name      the name of the attribute
	 * @param delimiter the delimiter to use between appended values
	 * @param value     the value of the attribute
	 */
	public HtmlAttribute(String name, char delimiter, String value) {
		this.name = name;
		this.delimiter = delimiter;
		this.value = value;
	}

	/**
	 * @return the name of the attribute
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the value of the attribute
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Appends a value to this attribute
	 * <p>
	 * If the existing value is not null, the new value is appended with the specified dilimiter inserted between values.
	 * </p>
	 *
	 * @param value the value to append
	 * @return this
	 */
	public HtmlAttribute appendValue(String value) {
		if (this.value == null) {
			this.value = value;
			return this;
		}
		this.value += delimiter + value;
		return this;
	}

	/**
	 * @return a properly-formatted attribute which may be embedded in a tag
	 */
	public String toHtmlString() {
		StringBuilder builder = new StringBuilder(name);
		if (value != null && !value.trim().isEmpty()) {
			builder.append("=\"").append(value).append("\"");
		}
		return builder.toString();
	}

	/**
	 * Creates a copy of this attribute
	 *
	 * @return the copied attribute
	 */
	public <T extends HtmlAttribute> T copy() {
		try {
			Class<T> clazz = (Class<T>) getClass();
			return HtmlAttribute.newAttribute(clazz, value);
		}
		catch (ReflectiveOperationException e) {
			throw new IllegalStateException(e);
		}
	}

	public static <T extends HtmlAttribute> T newAttribute(Class<T> clazz, String value) throws ReflectiveOperationException {
		Constructor<T> ctor = getConstructor(clazz, String.class);
		if (ctor != null) {
			return ctor.newInstance(value);
		}
		else {
			ctor = getConstructor(clazz, Number.class);
		}
		if (ctor != null) {
			return ctor.newInstance(Double.parseDouble(value));
		}
		else {
			ctor = getConstructor(clazz, boolean.class);
		}
		if (ctor != null) {
			return ctor.newInstance(Boolean.parseBoolean(value));
		}
		else {
			ctor = getConstructor(clazz);
		}
		if (ctor != null) {
			return ctor.newInstance();
		}
		else {
			throw new NoSuchMethodException("No viable constructor for attribute: " + clazz.getCanonicalName());
		}
	}

	private static <T> Constructor<T> getConstructor(Class<T> clazz, Class<?>... parameter_types) {
		try {
			Constructor<T> ctor = clazz.getDeclaredConstructor(parameter_types);
			ctor.setAccessible(true);
			return ctor;
		}
		catch (NoSuchMethodException e) {
			return null;
		}
	}
}
