package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_audio<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_audio() {
		super("audio");
	}

	public static final class AUTOPLAY extends HtmlAttribute {
		public AUTOPLAY() {
			super("autoplay", null);
		}
	}

	public static final class CONTROLS extends HtmlAttribute {
		public CONTROLS() {
			super("controls", null);
		}
	}

	public static final class LOOP extends HtmlAttribute {
		public LOOP() {
			super("loop", null);
		}
	}

	public static final class MUTED extends HtmlAttribute {
		public MUTED() {
			super("muted", null);
		}
	}

	public static final class PRELOAD extends HtmlAttribute {
		public PRELOAD() {
			super("preload", null);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}
}
