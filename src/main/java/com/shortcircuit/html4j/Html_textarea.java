package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_textarea extends HtmlContainer<HtmlWrapper> {

	public Html_textarea() {
		super("textarea");
	}

	public static final class AUTOFOCUS extends HtmlAttribute {
		public AUTOFOCUS() {
			super("autofocus", null);
		}
	}

	public static final class COLS extends HtmlAttribute {
		public COLS(Number value) {
			super("cols", value == null ? "0" : value.toString());
		}
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class MAXLENGTH extends HtmlAttribute {
		public MAXLENGTH(Number value) {
			super("maxlength", value == null ? "0" : value.toString());
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class PLACEHOLDER extends HtmlAttribute {
		public PLACEHOLDER(String value) {
			super("placeholder", value);
		}
	}

	public static final class READONLY extends HtmlAttribute {
		public READONLY() {
			super("readonly", null);
		}
	}

	public static final class REQUIRED extends HtmlAttribute {
		public REQUIRED() {
			super("required", null);
		}
	}

	public static final class ROWS extends HtmlAttribute {
		public ROWS(Number value) {
			super("rows", value == null ? "0" : value.toString());
		}
	}

	public static final class WRAP extends HtmlAttribute {
		public WRAP(_Wrap value) {
			super("wrap", value.wrap);
		}

		private WRAP(String value) {
			super("wrap", value);
		}
	}

	public enum _Wrap {
		HARD("hard"),
		SOFT("soft");

		private final String wrap;

		_Wrap(String wrap) {
			this.wrap = wrap;
		}
	}
}
