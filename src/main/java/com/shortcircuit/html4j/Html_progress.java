package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_progress extends HtmlContainer<HtmlWrapper> {

	public Html_progress() {
		super("progress");
	}

	public static final class MAX extends HtmlAttribute {
		public MAX(Number value) {
			super("max", value == null ? "0" : value.toString());
		}
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(Number value) {
			super("min", value == null ? "0" : value.toString());
		}
	}
}
