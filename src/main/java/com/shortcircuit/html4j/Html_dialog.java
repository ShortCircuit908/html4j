package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_dialog extends HtmlContainer<HtmlWrapper> {

	public Html_dialog() {
		super("dialog");
	}

	public static final class OPEN extends HtmlAttribute {
		public OPEN() {
			super("open", null);
		}
	}
}
