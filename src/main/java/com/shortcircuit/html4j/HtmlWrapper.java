package com.shortcircuit.html4j;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public abstract class HtmlWrapper {
	final ArrayList<HtmlAttribute> attributes = new ArrayList<>();
	final String type;
	private final boolean self_closing;

	/**
	 * Construct a new non-self-closing wrapper with the corresponding HTML element name
	 *
	 * @param type the name of the corresponding HTML element
	 */
	public HtmlWrapper(String type) {
		this(type, false);
	}

	/**
	 * Construct a new wrapper with the corresponding HTML element name
	 *
	 * @param self_closing if {@code true}, this element is regarded as self-closing and is not paired with a closing tag
	 * @param type         the name of the corresponding HTML element
	 */
	public HtmlWrapper(String type, boolean self_closing) {
		this.type = type;
		this.self_closing = self_closing;
	}

	/**
	 * @param attribute_class the class of the desired attribute
	 * @return {@code true} if the specified attribute is present
	 */
	public boolean hasAttribute(Class<? extends HtmlAttribute> attribute_class) {
		return getAttribute(attribute_class) != null;
	}

	/**
	 * @param attribute_class the class of the desired attribute
	 * @return the specified attribute
	 */
	public <T extends HtmlAttribute> T getAttribute(Class<T> attribute_class) {
		synchronized (this) {
			for (HtmlAttribute attribute : attributes) {
				if (attribute.getClass().equals(attribute_class)) {
					return (T) attribute;
				}
			}
			return null;
		}
	}

	/**
	 * Add an attribute to the element
	 * <p>
	 * If the element contains an identical attribute, the values of both attributes are appended.
	 * </p>
	 *
	 * @param attribute the attribute to add
	 * @return this
	 */
	public <T extends HtmlWrapper> T addAttribute(HtmlAttribute attribute) {
		synchronized (this) {
			HtmlAttribute temp_attribute = getAttribute(attribute.getClass());
			if (temp_attribute == null) {
				attributes.add(attribute);
			}
			else {
				temp_attribute.appendValue(attribute.getValue());
			}
			return (T) this;
		}
	}

	/**
	 * Remove an attribute from the element
	 *
	 * @param attribute_class the class of the attribute to remove
	 * @return {@code true} if the attribute was present
	 */
	public boolean clearAttribute(Class<? extends HtmlAttribute> attribute_class) {
		synchronized (this) {
			Iterator<HtmlAttribute> iterator = attributes.iterator();
			while (iterator.hasNext()) {
				if (iterator.next().getClass().equals(attribute_class)) {
					iterator.remove();
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Remove all attributes from the element
	 *
	 * @return this
	 */
	public <T extends HtmlWrapper> T clearAttributes() {
		synchronized (this) {
			attributes.clear();
			return (T) this;
		}
	}

	/**
	 * Add an attribute to the element
	 * <p>
	 * If the element contains an identical attribute, the existing attribute is overwritten
	 * </p>
	 *
	 * @param attribute the attribute to add
	 * @return this
	 */
	public <T extends HtmlWrapper> T setAttribute(HtmlAttribute attribute) {
		synchronized (this) {
			clearAttribute(attribute.getClass());
			addAttribute(attribute);
			return (T) this;
		}
	}

	/**
	 * Set the element's attributes
	 * <p>
	 * All existing attributes are cleared prior to adding the new ones
	 * </p>
	 *
	 * @param attributes the attributes to add
	 * @return this
	 */
	public <T extends HtmlWrapper> T setAttributes(HtmlAttribute... attributes) {
		synchronized (this) {
			this.attributes.clear();
			for (HtmlAttribute attribute : attributes) {
				addAttribute(attribute);
			}
			return (T) this;
		}
	}

	/**
	 * @return a properly-formatted HTML opening tag containing the element's name and attributes
	 */
	public String getOpeningTag() {
		synchronized (this) {
			StringBuilder builder = new StringBuilder("<").append(type);
			if (!attributes.isEmpty()) {
				for (HtmlAttribute attribute : attributes) {
					builder.append(' ').append(attribute.toHtmlString());
				}
			}
			if (self_closing) {
				builder.append('/');
			}
			return builder.append('>').toString();
		}
	}

	/**
	 * @return a properly-formatted HTML closing tag containing the element's name
	 */
	public String getClosingTag() {
		return self_closing ? "" : "</" + type + '>';
	}

	/**
	 * @return {@code true} if the tag is self-closing (is not paired with a closing tag)
	 */

	public boolean selfClosing() {
		return self_closing;
	}

	/**
	 * Creates a copy of this element and its attributes
	 *
	 * @return the copied element
	 */
	public <T extends HtmlWrapper> T copy() {
		synchronized (this) {
			try {
				try {
					Constructor<T> ctor = (Constructor<T>) getClass().getDeclaredConstructor();
					T copy = ctor.newInstance();
					for (HtmlAttribute attribute : attributes) {
						copy.setAttribute(attribute.copy());
					}
					return copy;
				}
				catch (NoSuchMethodException e) {
					Constructor<T> ctor = (Constructor<T>) getClass().getDeclaredConstructor(String.class, boolean.class);
					T copy = ctor.newInstance(type, self_closing);
					for (HtmlAttribute attribute : attributes) {
						copy.setAttribute(attribute.copy());
					}
					return copy;
				}
			}
			catch (ReflectiveOperationException e) {
				throw new IllegalStateException(e);
			}
		}
	}

	/**
	 * @return a string containing properly-formatted HTML which represents this and any sub-elements
	 */
	public abstract String toHtmlString();

	public static final class ACCESSKEY extends HtmlAttribute {
		public ACCESSKEY(String value) {
			super("accesskey", value);
		}
	}

	public static final class CLASS extends HtmlAttribute {
		public CLASS(String value) {
			super("class", value);
		}
	}

	public static final class CONTENTEDITABLE extends HtmlAttribute {
		public CONTENTEDITABLE(boolean value) {
			super("contenteditable", value + "");
		}
	}

	public static final class CONTEXTMENU extends HtmlAttribute {
		public CONTEXTMENU(String value) {
			super("contextmenu", value);
		}
	}

	public static final class DATA extends HtmlAttribute {
		public DATA(String data_type, String value) {
			super("data-" + data_type, value);
		}
	}

	public static final class DIR extends HtmlAttribute {
		public DIR(_Flow value) {
			super("dir", value.flow);
		}

		private DIR(String value){
			super("dir", value);
		}
	}

	public static final class DRAGGABLE extends HtmlAttribute {
		public DRAGGABLE(boolean value) {
			super("draggable", value + "");
		}
	}

	public static final class DROPZONE extends HtmlAttribute {
		public DROPZONE(_Dropzone value) {
			super("dropzone", value.dropzone);
		}

		private DROPZONE(String value) {
			super("dropzone", value);
		}
	}

	public static final class HIDDEN extends HtmlAttribute {
		public HIDDEN() {
			super("hidden", null);
		}
	}

	public static final class ID extends HtmlAttribute {
		public ID(String value) {
			super("id", value);
		}
	}

	public static final class LANG extends HtmlAttribute {
		public LANG(String value) {
			super("lang", value);
		}
	}

	public static final class SPELLCHECK extends HtmlAttribute {
		public SPELLCHECK(boolean value) {
			super("spellcheck", value + "");
		}
	}

	public static final class STYLE extends HtmlAttribute {
		public STYLE(String value) {
			super("style", value);
		}
	}

	public static final class TABINDEX extends HtmlAttribute {
		public TABINDEX(Number value) {
			super("tabindex", value == null ? "0" : value.toString());
		}
	}

	public static final class TITLE extends HtmlAttribute {
		public TITLE(String value) {
			super("title", value);
		}
	}

	public static final class TRANSLATE extends HtmlAttribute {
		public TRANSLATE(boolean value) {
			super("translate", value ? "yes" : "no");
		}
	}

	public enum _Flow {
		LEFT_TO_RIGHT("ltr"),
		RIGHT_TO_LEFT("rtl"),
		AUTO("auto");
		final String flow;

		_Flow(String flow) {
			this.flow = flow;
		}
	}

	public enum _Dropzone {
		COPY("copy"),
		MOVE("move"),
		LINK("link");

		final String dropzone;

		_Dropzone(String dropzone) {
			this.dropzone = dropzone;
		}
	}

	public static final class ONAFTERPRINT extends HtmlAttribute {
		public ONAFTERPRINT(String value) {
			super("onafterprint", value);
		}
	}

	public static final class ONBEFOREPRINT extends HtmlAttribute {
		public ONBEFOREPRINT(String value) {
			super("onbeforeprint", value);
		}
	}

	public static final class ONBEFOREUNLOAD extends HtmlAttribute {
		public ONBEFOREUNLOAD(String value) {
			super("onbeforeunload", value);
		}
	}

	public static final class ONERROR extends HtmlAttribute {
		public ONERROR(String value) {
			super("onerror", value);
		}
	}

	public static final class ONHASHCHANGE extends HtmlAttribute {
		public ONHASHCHANGE(String value) {
			super("onhashchange", value);
		}
	}

	public static final class ONLOAD extends HtmlAttribute {
		public ONLOAD(String value) {
			super("onload", value);
		}
	}

	public static final class ONMESSAGE extends HtmlAttribute {
		public ONMESSAGE(String value) {
			super("onmessage", value);
		}
	}

	public static final class ONOFFLINE extends HtmlAttribute {
		public ONOFFLINE(String value) {
			super("onoffline", value);
		}
	}

	public static final class ONONLINE extends HtmlAttribute {
		public ONONLINE(String value) {
			super("ononline", value);
		}
	}

	public static final class ONPAGEHIDE extends HtmlAttribute {
		public ONPAGEHIDE(String value) {
			super("onpagehide", value);
		}
	}

	public static final class ONPAGESHOW extends HtmlAttribute {
		public ONPAGESHOW(String value) {
			super("onpageshow", value);
		}
	}

	public static final class ONPOPSTATE extends HtmlAttribute {
		public ONPOPSTATE(String value) {
			super("onpopstate", value);
		}
	}

	public static final class ONRESIZE extends HtmlAttribute {
		public ONRESIZE(String value) {
			super("onresize", value);
		}
	}

	public static final class ONSTORAGE extends HtmlAttribute {
		public ONSTORAGE(String value) {
			super("onstorage", value);
		}
	}

	public static final class ONUNLOAD extends HtmlAttribute {
		public ONUNLOAD(String value) {
			super("onunload", value);
		}
	}

	public static final class ONBLUR extends HtmlAttribute {
		public ONBLUR(String value) {
			super("onblur", value);
		}
	}

	public static final class ONCHANGE extends HtmlAttribute {
		public ONCHANGE(String value) {
			super("onchange", value);
		}
	}

	public static final class ONCONTEXTMENU extends HtmlAttribute {
		public ONCONTEXTMENU(String value) {
			super("oncontextmenu", value);
		}
	}

	public static final class ONFOCUS extends HtmlAttribute {
		public ONFOCUS(String value) {
			super("onfocus", value);
		}
	}

	public static final class ONINPUT extends HtmlAttribute {
		public ONINPUT(String value) {
			super("oninput", value);
		}
	}

	public static final class ONINVALID extends HtmlAttribute {
		public ONINVALID(String value) {
			super("oninvalid", value);
		}
	}

	public static final class ONRESET extends HtmlAttribute {
		public ONRESET(String value) {
			super("onreset", value);
		}
	}

	public static final class ONSEARCH extends HtmlAttribute {
		public ONSEARCH(String value) {
			super("onsearch", value);
		}
	}

	public static final class ONSELECT extends HtmlAttribute {
		public ONSELECT(String value) {
			super("onselect", value);
		}
	}

	public static final class ONSUBMIT extends HtmlAttribute {
		public ONSUBMIT(String value) {
			super("onsubmit", value);
		}
	}

	public static final class ONKEYDOWN extends HtmlAttribute {
		public ONKEYDOWN(String value) {
			super("onkeydown", value);
		}
	}

	public static final class ONKEYPRESS extends HtmlAttribute {
		public ONKEYPRESS(String value) {
			super("onkeypress", value);
		}
	}

	public static final class ONKEYUP extends HtmlAttribute {
		public ONKEYUP(String value) {
			super("onkeyup", value);
		}
	}

	public static final class ONCLICK extends HtmlAttribute {
		public ONCLICK(String value) {
			super("onclick", value);
		}
	}

	public static final class ONDBLCLICK extends HtmlAttribute {
		public ONDBLCLICK(String value) {
			super("ondblclick", value);
		}
	}

	public static final class ONDRAG extends HtmlAttribute {
		public ONDRAG(String value) {
			super("ondrag", value);
		}
	}

	public static final class ONDRAGEND extends HtmlAttribute {
		public ONDRAGEND(String value) {
			super("ondragend", value);
		}
	}

	public static final class ONDRAGENTER extends HtmlAttribute {
		public ONDRAGENTER(String value) {
			super("ondragenter", value);
		}
	}

	public static final class ONDRAGLEAVE extends HtmlAttribute {
		public ONDRAGLEAVE(String value) {
			super("ondragleave", value);
		}
	}

	public static final class ONDRAGOVER extends HtmlAttribute {
		public ONDRAGOVER(String value) {
			super("ondragover", value);
		}
	}

	public static final class ONDRAGSTART extends HtmlAttribute {
		public ONDRAGSTART(String value) {
			super("ondragstart", value);
		}
	}

	public static final class ONDROP extends HtmlAttribute {
		public ONDROP(String value) {
			super("ondrop", value);
		}
	}

	public static final class ONMOUSEDOWN extends HtmlAttribute {
		public ONMOUSEDOWN(String value) {
			super("onmousedown", value);
		}
	}

	public static final class ONMOUSEMOVE extends HtmlAttribute {
		public ONMOUSEMOVE(String value) {
			super("onmousemove", value);
		}
	}

	public static final class ONMOUSEOUT extends HtmlAttribute {
		public ONMOUSEOUT(String value) {
			super("onmouseout", value);
		}
	}

	public static final class ONMOUSEOVER extends HtmlAttribute {
		public ONMOUSEOVER(String value) {
			super("onmouseover", value);
		}
	}

	public static final class ONMOUSEUP extends HtmlAttribute {
		public ONMOUSEUP(String value) {
			super("onmouseup", value);
		}
	}

	public static final class ONSCROLL extends HtmlAttribute {
		public ONSCROLL(String value) {
			super("onscroll", value);
		}
	}

	public static final class ONWHEEL extends HtmlAttribute {
		public ONWHEEL(String value) {
			super("onwheel", value);
		}
	}

	public static final class ONCOPY extends HtmlAttribute {
		public ONCOPY(String value) {
			super("oncopy", value);
		}
	}

	public static final class ONCUT extends HtmlAttribute {
		public ONCUT(String value) {
			super("oncut", value);
		}
	}

	public static final class ONPASTE extends HtmlAttribute {
		public ONPASTE(String value) {
			super("onpaste", value);
		}
	}

	public static final class ONABORT extends HtmlAttribute {
		public ONABORT(String value) {
			super("onabort", value);
		}
	}

	public static final class ONCANPLAY extends HtmlAttribute {
		public ONCANPLAY(String value) {
			super("oncanplay", value);
		}
	}

	public static final class ONCANPLAYTHROUGH extends HtmlAttribute {
		public ONCANPLAYTHROUGH(String value) {
			super("oncanplaythrough", value);
		}
	}

	public static final class ONCUECHANGE extends HtmlAttribute {
		public ONCUECHANGE(String value) {
			super("oncuechange", value);
		}
	}

	public static final class ONDURATIONCHANGE extends HtmlAttribute {
		public ONDURATIONCHANGE(String value) {
			super("ondurationchange", value);
		}
	}

	public static final class ONEMPTIED extends HtmlAttribute {
		public ONEMPTIED(String value) {
			super("onemptied", value);
		}
	}

	public static final class ONENDED extends HtmlAttribute {
		public ONENDED(String value) {
			super("onended", value);
		}
	}

	public static final class ONLOADEDDATA extends HtmlAttribute {
		public ONLOADEDDATA(String value) {
			super("onloadeddata", value);
		}
	}

	public static final class ONLOADEDMETADATA extends HtmlAttribute {
		public ONLOADEDMETADATA(String value) {
			super("onloadedmetadata", value);
		}
	}

	public static final class ONLOADSTART extends HtmlAttribute {
		public ONLOADSTART(String value) {
			super("onloadstart", value);
		}
	}

	public static final class ONPAUSE extends HtmlAttribute {
		public ONPAUSE(String value) {
			super("onpause", value);
		}
	}

	public static final class ONPLAY extends HtmlAttribute {
		public ONPLAY(String value) {
			super("onplay", value);
		}
	}

	public static final class ONPLAYING extends HtmlAttribute {
		public ONPLAYING(String value) {
			super("onplaying", value);
		}
	}

	public static final class ONPROGRESS extends HtmlAttribute {
		public ONPROGRESS(String value) {
			super("onprogress", value);
		}
	}

	public static final class ONRATECHANGE extends HtmlAttribute {
		public ONRATECHANGE(String value) {
			super("onratechange", value);
		}
	}

	public static final class ONSEEKED extends HtmlAttribute {
		public ONSEEKED(String value) {
			super("onseeked", value);
		}
	}

	public static final class ONSEEKING extends HtmlAttribute {
		public ONSEEKING(String value) {
			super("onseeking", value);
		}
	}

	public static final class ONSTALLED extends HtmlAttribute {
		public ONSTALLED(String value) {
			super("onstalled", value);
		}
	}

	public static final class ONSUSPEND extends HtmlAttribute {
		public ONSUSPEND(String value) {
			super("onsuspend", value);
		}
	}

	public static final class ONTIMEUPDATE extends HtmlAttribute {
		public ONTIMEUPDATE(String value) {
			super("ontimeupdate", value);
		}
	}

	public static final class ONVOLUMECHANGE extends HtmlAttribute {
		public ONVOLUMECHANGE(String value) {
			super("onvolumechange", value);
		}
	}

	public static final class ONWAITING extends HtmlAttribute {
		public ONWAITING(String value) {
			super("onwaiting", value);
		}
	}

	public static final class ONSHOW extends HtmlAttribute {
		public ONSHOW(String value) {
			super("onshow", value);
		}
	}

	public static final class ONTOGGLE extends HtmlAttribute {
		public ONTOGGLE(String value) {
			super("ontoggle", value);
		}
	}
}
