package com.shortcircuit.html4j;

import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_script<T extends HtmlWrapper> extends HtmlContainer<T> {

	public Html_script() {
		super("script");
	}

	public static final class ASYNC extends HtmlAttribute {
		public ASYNC() {
			super("async", null);
		}
	}

	public static final class CHARSET extends HtmlAttribute {
		public CHARSET(Charset value) {
			super("charset", value.name());
		}

		public CHARSET(String value) throws UnsupportedCharsetException {
			super("charset", Charset.forName(value).name());
		}
	}

	public static final class DEFER extends HtmlAttribute {
		public DEFER() {
			super("defer", null);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}
}
