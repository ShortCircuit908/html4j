package com.shortcircuit.html4j;

import java.util.*;

/**
 * Represents an HTML element which may contain any number of elements within itself
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
@SuppressWarnings("NullableProblems")
public abstract class HtmlContainer<T extends HtmlWrapper> extends HtmlWrapper implements List<T> {
	final ArrayList<T> children = new ArrayList<>();

	/**
	 * Construct a new non-self-closing wrapper with the corresponding HTML element name
	 *
	 * @param type the name of the corresponding HTML element
	 */
	public HtmlContainer(String type) {
		super(type);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHtmlString() {
		synchronized (this) {
			StringBuilder builder = new StringBuilder(getOpeningTag());
			for (T child : children) {
				builder.append(child.toHtmlString());
			}
			return builder.append(getClosingTag()).toString();
		}
	}

	/**
	 * Creates a copy of this element and its attributes, as well as any elements contained within
	 *
	 * @return the copied element
	 * @see #deepCopy(int)
	 */
	public <H extends HtmlContainer<T>> H deepCopy() {
		return deepCopy(-1);
	}

	/**
	 * Creates a copy of this element and its attributes, as well as any elements contained within
	 * <p>
	 * The HTML value of the copy will be identical to the source's, and will contain copies up to {@code n}-layer nested elements.<br>
	 * For example, calling {@link #deepCopy(int) deepCopy(1} on
	 * <p><pre>
	 * <!div class="class1" style="background:red">
	 *     <!h1 style="color:blue">Header<!/h1>
	 *     <!div class="class2">
	 *         <!p>
	 *             Some text
	 *         </!p>
	 *     <!/div>
	 * <!/div>
	 * </pre></p>
	 * will create a copy resembling
	 * <p><pre>
	 * <!div class="class1" style="background:red">
	 *     <!h1 style="color:blue">Header<!/h1>
	 *     <!div class="class2">
	 *     <!/div>
	 * <!/div>
	 * </pre></p><br>
	 * An argument less than 0 will copy <strong>all</strong> nested elements.
	 * </p>
	 *
	 * @param level how many levels of elements will be copied
	 * @return the copied element
	 */
	public <H extends HtmlContainer<T>> H deepCopy(int level) {
		synchronized (this) {
			HtmlContainer<T> copy = copy();
			if (level != 0) {
				for (T child : children) {
					if (child instanceof HtmlContainer) {
						copy.add((T) ((HtmlContainer) child).deepCopy(level - 1));
					}
					else {
						copy.add((T) child.copy());
					}
				}
			}
			return (H) copy;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return children.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return children.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object o) {
		return children.contains(o);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator() {
		return children.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toArray() {
		return children.toArray();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T[] toArray(T[] a) {
		return children.toArray(a);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean add(T e) {
		synchronized (this) {
			return children.add(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object o) {
		synchronized (this) {
			return children.remove(o);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		return children.containsAll(c);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(Collection<? extends T> c) {
		synchronized (this) {
			return children.addAll(c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		synchronized (this) {
			return children.addAll(index, c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		synchronized (this) {
			return children.removeAll(c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		synchronized (this) {
			return children.retainAll(c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		synchronized (this) {
			children.clear();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get(int index) {
		synchronized (this) {
			return children.get(index);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T set(int index, T element) {
		synchronized (this) {
			return children.set(index, element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(int index, T element) {
		synchronized (this) {
			children.add(index, element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T remove(int index) {
		synchronized (this) {
			return children.remove(index);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int indexOf(Object o) {
		return children.indexOf(o);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int lastIndexOf(Object o) {
		return children.lastIndexOf(o);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListIterator<T> listIterator() {
		return children.listIterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListIterator<T> listIterator(int index) {
		return children.listIterator(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		synchronized (this) {
			return children.subList(fromIndex, toIndex);
		}
	}
}
