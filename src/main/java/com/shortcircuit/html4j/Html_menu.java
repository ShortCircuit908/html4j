package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_menu<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_menu() {
		super("menu");
	}

	public static final class LABEL extends HtmlAttribute {
		public LABEL(String value) {
			super("label", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(_Type value) {
			super("type", value.type);
		}

		private TYPE(String value) {
			super("type", value);
		}
	}

	public enum _Type {
		POPUP("popup"),
		TOOLBAR("toolbar"),
		CONTEXT("context");

		private final String type;

		_Type(String type) {
			this.type = type;
		}
	}
}
