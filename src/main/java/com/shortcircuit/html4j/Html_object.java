package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_object extends HtmlWrapper {
	public Html_object() {
		super("object");
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class DATA extends HtmlAttribute {
		public DATA(String value) {
			super("data", value);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class HEIGHT extends HtmlAttribute {
		public HEIGHT(Number value) {
			super("height", value == null ? "0" : value.toString());
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}

	public static final class USEMAP extends HtmlAttribute {
		public USEMAP(String value) {
			super("src", value);
		}
	}

	public static final class WIDTH extends HtmlAttribute {
		public WIDTH(Number value) {
			super("width", value == null ? "0" : value.toString());
		}
	}
}
