package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/12/2015
 */
public class Html_h4<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_h4() {
		super("h4");
	}
}
