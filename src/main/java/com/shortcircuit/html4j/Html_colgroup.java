package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_colgroup extends HtmlWrapper {
	public Html_colgroup() {
		super("colgroup", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class SPAN extends HtmlAttribute {
		public SPAN(Number value) {
			super("span", value == null ? "0" : value.toString());
		}
	}
}
