package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_option<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_option() {
		super("option");
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class LABEL extends HtmlAttribute {
		public LABEL(String value) {
			super("label", value);
		}
	}

	public static final class SELECTED extends HtmlAttribute {
		public SELECTED() {
			super("selected", null);
		}
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(String value) {
			super("value", value);
		}
	}
}
