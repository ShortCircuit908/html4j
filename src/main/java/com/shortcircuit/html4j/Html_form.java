package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_form extends HtmlContainer<HtmlWrapper> {

	public Html_form() {
		super("form");
	}

	public static final class ACTION extends HtmlAttribute {
		public ACTION(String value) {
			super("action", value);
		}
	}

	public static final class AUTOCOMPLETE extends HtmlAttribute {
		public AUTOCOMPLETE(boolean value) {
			super("autocomplete", value ? "on" : "off");
		}
	}

	public static final class ENCTYPE extends HtmlAttribute {
		public ENCTYPE(_EncType value) {
			super("enctype", value.enctype);
		}

		private ENCTYPE(String value) {
			super("enctype", value);
		}
	}

	public static final class METHOD extends HtmlAttribute {
		public METHOD(_Method value) {
			super("method", value.method);
		}

		private METHOD(String value) {
			super("method", value);
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class NOVALIDATE extends HtmlAttribute {
		public NOVALIDATE() {
			super("novalidate", null);
		}
	}

	public static final class TARGET extends HtmlAttribute {
		public TARGET(_Target value) {
			super("target", value.target);
		}

		private TARGET(String value) {
			super("target", value);
		}
	}

	public enum _EncType {
		APPLICATION("application/x-www-form-urlencoded"),
		MULTIPART("multipart/form-data"),
		PLAIN_TEXT("text/plain");

		private final String enctype;

		_EncType(String enctype) {
			this.enctype = enctype;
		}
	}

	public enum _Method {
		GET("get"),
		POST("post");

		private final String method;

		_Method(String method) {
			this.method = method;
		}
	}

	public enum _Target {
		BLANK("_blank"),
		PARENT("_parent"),
		SELF("_self"),
		TOP("_top");
		private final String target;

		_Target(String target) {
			this.target = target;
		}
	}

}
