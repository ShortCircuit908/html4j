package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_iframe<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_iframe() {
		super("iframe");
	}

	public static final class HEIGHT extends HtmlAttribute {
		public HEIGHT(Number value) {
			super("height", value == null ? "0" : value.toString());
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class SANDBOX extends HtmlAttribute {
		public SANDBOX(_Sandbox value) {
			super("sandbox", value.sandbox);
		}

		private SANDBOX(String value) {
			super("sandbox", value);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class SRCDOC extends HtmlAttribute {
		public SRCDOC(HtmlWrapper value) {
			super("srcdoc", value.toHtmlString());
		}

		private SRCDOC(String value) {
			super("srcdoc", value);
		}
	}

	public static final class WIDTH extends HtmlAttribute {
		public WIDTH(Number value) {
			super("width", value == null ? "0" : value.toString());
		}
	}

	public enum _Sandbox {
		ALLOW_FORMS("allow-forms"),
		ALLOW_POINTER_LOCK("allow-pointer-lock"),
		ALLOW_POPUPS("allow-popups"),
		ALLOW_SAME_ORIGIN("allow-same-origin"),
		ALLOW_SCRIPTS("allow-scripts"),
		ALLOW_TOP_NAVICATION("allow-top-navigation");

		private final String sandbox;

		_Sandbox(String sandbox) {
			this.sandbox = sandbox;
		}
	}
}
