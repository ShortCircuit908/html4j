package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_keygen extends HtmlContainer<HtmlWrapper> {

	public Html_keygen() {
		super("keygen");
	}

	public static final class AUTOFOCUS extends HtmlAttribute {
		public AUTOFOCUS() {
			super("autofocus", null);
		}
	}

	public static final class CHALLENGE extends HtmlAttribute {
		public CHALLENGE() {
			super("challenge", null);
		}
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class KEYTYPE extends HtmlAttribute {
		public KEYTYPE(_KeyType value) {
			super("keytype", value.keytype);
		}

		private KEYTYPE(String value) {
			super("keytype", value);
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public enum _KeyType {
		RSA("rsa"),
		DSA("dsa"),
		EC("ec");

		private final String keytype;

		_KeyType(String keytype) {
			this.keytype = keytype;
		}
	}
}
