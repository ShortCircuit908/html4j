package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_meter extends HtmlContainer<HtmlWrapper> {

	public Html_meter() {
		super("meter");
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class HIGH extends HtmlAttribute {
		public HIGH(Number value) {
			super("high", value == null ? "0" : value.toString());
		}
	}

	public static final class LOW extends HtmlAttribute {
		public LOW(Number value) {
			super("low", value == null ? "0" : value.toString());
		}
	}

	public static final class MAX extends HtmlAttribute {
		public MAX(Number value) {
			super("max", value == null ? "0" : value.toString());
		}
	}

	public static final class MIN extends HtmlAttribute {
		public MIN(Number value) {
			super("min", value == null ? "0" : value.toString());
		}
	}

	public static final class OPTIMUM extends HtmlAttribute {
		public OPTIMUM(Number value) {
			super("min", value == null ? "0" : value.toString());
		}
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(String value) {
			super("value", value);
		}
	}
}
