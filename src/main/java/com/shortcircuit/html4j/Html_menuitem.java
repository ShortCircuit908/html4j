package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_menuitem<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_menuitem() {
		super("menuitem");
	}

	public static final class CHECKED extends HtmlAttribute {
		public CHECKED() {
			super("checked", null);
		}
	}

	public static final class COMMAND extends HtmlAttribute {
		public COMMAND(String value) {
			super("command", value);
		}
	}

	public static final class DEFAULT extends HtmlAttribute {
		public DEFAULT() {
			super("default", null);
		}
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class ICON extends HtmlAttribute {
		public ICON(String value) {
			super("icon", value);
		}
	}

	public static final class LABEL extends HtmlAttribute {
		public LABEL(String value) {
			super("label", value);
		}
	}

	public static final class RADIOGROUP extends HtmlAttribute {
		public RADIOGROUP(String value) {
			super("radiogroup", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(_Type value) {
			super("type", value.type);
		}

		private TYPE(String value) {
			super("type", value);
		}
	}

	public enum _Type {
		CHECKBOX("checkbox"),
		COMMAND("command"),
		RADIO("radio");

		private final String type;

		_Type(String type) {
			this.type = type;
		}
	}
}
