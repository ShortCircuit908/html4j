package com.shortcircuit.html4j.page;

import com.shortcircuit.html4j.HtmlWrapper;
import com.shortcircuit.html4j.Html_html;
import com.shortcircuit.html4j.parse.Parser;
import org.eclipse.jetty.server.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Represents a resource containing an HTML document
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class HtmlResource extends Resource {
	private final Html_html<HtmlWrapper> document = new Html_html<>();

	/**
	 * Initialize a new HTML resource with a given name<br>
	 * <p>
	 * Resource names are case-sensitive but ignore leading and trailing slashes.<br>
	 * Inserting a slash into the name indicates that the resource is located within a virtual subdirectory.
	 * </p>
	 *
	 * @param name the name of the resource
	 */
	public HtmlResource(String name) {
		super(name, "text/html");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean writeResource(Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException{
		PrintWriter writer = new PrintWriter(response.getOutputStream());
		writer.write(toHtmlString());
		writer.flush();
		writer.close();
		response.setStatus(HttpServletResponse.SC_OK);
		return true;
	}

	/**
	 * @return the outermost element of the document
	 */
	public Html_html<HtmlWrapper> getDocument() {
		return document;
	}

	/**
	 * @return a properly-formatted HTML document
	 */
	public String toHtmlString() {
		return "<!DOCTYPE html>" + Parser.formatHtml(document.toHtmlString(), true);
	}
}
