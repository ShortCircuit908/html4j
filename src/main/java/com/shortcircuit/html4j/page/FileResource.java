package com.shortcircuit.html4j.page;

import org.eclipse.jetty.server.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Represents a resource located on the filesystem
 *
 * @author ShortCircuit908
 *         Created on 10/8/2015
 */
public class FileResource extends Resource {
	private final File file;

	/**
	 * Initialize a new file resource with a given source file, name, and MIME type<br>
	 * <p>
	 * Resource names are case-sensitive but ignore leading and trailing slashes.<br>
	 * Inserting a slash into the name indicates that the resource is located within a virtual subdirectory.
	 * </p>
	 *
	 * @param file         the source file of the resource
	 * @param name         the name of the resource
	 * @param content_type the indicated MIME type of the resource
	 */
	public FileResource(File file, String name, String content_type) {
		super(name, content_type);
		this.file = file;
	}

	/**
	 * Initialize a new file resource with a given source file, name. The MIME type will be inferenced<br>
	 * <p>
	 * Resource names are case-sensitive but ignore leading and trailing slashes.<br>
	 * Inserting a slash into the name indicates that the resource is located within a virtual subdirectory.
	 * </p>
	 *
	 * @param file the source file of the resource
	 * @param name the name of the resource
	 */
	public FileResource(File file, String name) throws IOException {
		super(name, Files.probeContentType(file.toPath()));
		this.file = file;
	}

	public File getFile(){
		return file;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean writeResource(Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException{
		copyStream(new FileInputStream(file), response.getOutputStream());
		response.setStatus(HttpServletResponse.SC_OK);
		return true;
	}
}
