package com.shortcircuit.html4j.page;

import org.eclipse.jetty.server.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Represents a resource located within the application's classpath
 *
 * @author ShortCircuit908
 *         Created on 10/8/2015
 */
public class ClasspathResource extends Resource {
	private final ClassLoader context_loader;
	private final String classpath;

	/**
	 * Initialize a new classpath resource with a given source path, name, and MIME type<br>
	 * <p>
	 * Resource names are case-sensitive but ignore leading and trailing slashes.<br>
	 * Inserting a slash into the name indicates that the resource is located within a virtual subdirectory.
	 * </p>
	 *
	 * @param context      any class within the source's classpath
	 * @param classpath    location of the resource file within the classpath
	 * @param name         the name of the resource
	 * @param content_type the indicated MIME type of the resource
	 */
	public ClasspathResource(Class context, String classpath, String name, String content_type) {
		super(name, content_type);
		context_loader = context.getClassLoader();
		this.classpath = classpath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean writeResource(Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException {
		copyStream(context_loader.getResourceAsStream(classpath), response.getOutputStream());
		response.setStatus(HttpServletResponse.SC_OK);
		return true;
	}
}
