package com.shortcircuit.html4j.page;

import com.shortcircuit.html4j.jetty.ResourceHandler;
import org.eclipse.jetty.server.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents a virtual resource for use with a {@link ResourceHandler ResourceHandler}<br>
 * <p>
 * When added to an active ResourceHandler, the resource is accessible via the URL {@code base_url/resourcename}.
 * </p>
 *
 * @author ShortCircuit908
 *         Created on 10/8/2015
 */
public abstract class Resource {
	private final String name;
	private final String content_type;

	/**
	 * Initialize a new resource with a given name and MIME type<br>
	 * <p>
	 * Resource names are case-sensitive but ignore leading and trailing slashes.<br>
	 * Inserting a slash into the name indicates that the resource is located within a virtual subdirectory.
	 * </p>
	 *
	 * @param name         the name of the resource
	 * @param content_type the indicated MIME type of the resource
	 */
	public Resource(String name, String content_type) {
		this.name = name.substring(name.startsWith("/") ? 1 : 0, name.endsWith("/") ? name.length() - 1 : name.length());
		this.content_type = content_type;
	}

	/**
	 * @return the resource's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the resource's indicated MIME type
	 */
	public String getContentType() {
		return content_type;
	}

	/**
	 * Method called when the resource's content is requested
	 *
	 * @param base_request
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public abstract boolean writeResource(Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException;

	protected void copyStream(InputStream src, OutputStream dest) throws IOException {
		int n;
		byte[] buffer = new byte[1024];
		while ((n = src.read(buffer)) > -1) {
			dest.write(buffer, 0, n);
		}
		dest.flush();
		src.close();
	}
}