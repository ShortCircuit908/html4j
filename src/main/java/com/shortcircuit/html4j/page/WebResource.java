package com.shortcircuit.html4j.page;

import org.eclipse.jetty.server.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;

/**
 * Represents a resource accessible by URL
 *
 * @author ShortCircuit908
 *         Created on 10/9/2015
 */
public class WebResource extends Resource {
	private final URL url;

	/**
	 * Initialize a new web resource with a given source URL, name, and MIME type<br>
	 * <p>
	 * Resource names are case-sensitive but ignore leading and trailing slashes.<br>
	 * Inserting a slash into the name indicates that the resource is located within a virtual subdirectory.
	 * </p>
	 *
	 * @param url          a valid URL which points to the resource
	 * @param name         the name of the resource
	 * @param content_type the indicated MIME type of the resource
	 */
	public WebResource(URL url, String name, String content_type) {
		super(name, content_type);
		this.url = url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean writeResource(Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException {
		copyStream(url.openStream(), response.getOutputStream());
		response.setStatus(HttpServletResponse.SC_OK);
		return true;
	}
}