package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_track<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_track() {
		super("track");
	}

	public static final class DEFAULT extends HtmlAttribute {
		public DEFAULT() {
			super("default", null);
		}
	}

	public static final class KIND extends HtmlAttribute {
		public KIND(_Kind value) {
			super("kind", value.kind);
		}

		private KIND(String value) {
			super("kind", value);
		}
	}

	public static final class LABEL extends HtmlAttribute {
		public LABEL(String value) {
			super("label", value);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class SRCLANG extends HtmlAttribute {
		public SRCLANG(String value) {
			super("srclang", value);
		}
	}

	public enum _Kind {
		CAPTIONS("captions"),
		CHAPTERS("chapters"),
		DESCRIPTIONS("descriptions"),
		METADATA("metadata"),
		SUBTITLES("subtitles");

		private final String kind;

		_Kind(String kind) {
			this.kind = kind;
		}
	}
}
