package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_a<T extends HtmlWrapper> extends HtmlContainer<T> {

	public Html_a() {
		super("a");
	}

	public static final class DOWNLOAD extends HtmlAttribute {
		public DOWNLOAD(String value) {
			super("download", value);
		}
	}

	public static final class HREF extends HtmlAttribute {
		public HREF(String value) {
			super("href", value);
		}
	}

	public static final class HREFLANG extends HtmlAttribute {
		public HREFLANG(String value) {
			super("hreflang", value);
		}
	}

	public static final class MEDIA extends HtmlAttribute {
		public MEDIA(String value) {
			super("media", value);
		}
	}

	public static final class REL extends HtmlAttribute {
		public REL(_Rel value) {
			this(value.rel);
		}

		public REL(String value) {
			super("rel", value);
		}
	}

	public static final class TARGET extends HtmlAttribute {
		public TARGET(_Target value) {
			this(value.target);
		}

		public TARGET(String value) {
			super("target", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}

	public enum _Rel {
		ALTERNATE("alternate"),
		AUTHOR("author"),
		BOOKMARK("bookmark"),
		HELP("help"),
		LICENSE("license"),
		NEXT("next"),
		NOFOLLOW("nofollow"),
		NOREFERRER("noreferrer"),
		PREFETCH("prefetch"),
		PREV("prev"),
		SEARCH("search"),
		TAG("tag");

		private final String rel;

		_Rel(String rel) {
			this.rel = rel;
		}
	}

	public enum _Target {
		BLANK("_blank"),
		PARENT("_parent"),
		SELF("_self"),
		TOP("_top");
		private final String target;

		_Target(String target) {
			this.target = target;
		}
	}
}
