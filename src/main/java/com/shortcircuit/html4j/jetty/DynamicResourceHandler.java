package com.shortcircuit.html4j.jetty;


import com.shortcircuit.html4j.page.Resource;
import org.eclipse.jetty.server.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class DynamicResourceHandler extends Resource {
	private final ArrayList<Resource> resources = new ArrayList<>();
	private final String parameter_name;
	private String home_page = "index.html";

	public DynamicResourceHandler(String name, String parameter_name) {
		super(name, "unknown");
		this.parameter_name = parameter_name;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean writeResource(Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String parameter = base_request.getParameter(this.parameter_name);
		if (parameter == null) {
			parameter = this.home_page;
		}

		Resource resource = this.getResource(parameter);
		if (resource == null) {
			response.setStatus(404);
			return false;
		}
		else {
			response.setContentType(resource.getContentType());
			return resource.writeResource(base_request, request, response);
		}
	}

	/**
	 * Sets this handler's "home page"<br>
	 * <p>
	 * The home page is a resource which will be served in response to a request for the root target
	 * </p>
	 *
	 * @param home_page the name of the resource
	 */
	public void setHomePage(String home_page) {
		this.home_page = home_page;
	}

	/**
	 * Get a resource by its name<br>
	 * <p>
	 * This method ignores leading and trailing {@code /} characters
	 * </p>
	 *
	 * @param name the name of the resource
	 * @return the specified resource
	 */

	public Resource getResource(String name) {
		if (name == null) {
			return null;
		}
		name = name.substring(name.startsWith("/") ? 1 : 0, name.endsWith("/") ? name.length() - 1 : name.length());
		synchronized (this) {
			for (Resource resource : resources) {
				if (resource.getName().equals(name)) {
					return resource;
				}
			}
			return null;
		}
	}

	/**
	 * Add a resource to the store<br>
	 * <p>
	 * If the handler contains a resource with an identical name, the existing resource is replaced and returned.
	 * </p>
	 *
	 * @param resource the new resource to add
	 * @return the overwritten resource
	 */
	public Resource addResource(Resource resource) {
		synchronized (this) {
			Resource removed = removeResource(resource.getName());
			resources.add(resource);
			return removed;
		}
	}

	/**
	 * Remove a resource from the store<br>
	 *
	 * @param name the name of the resource
	 * @return the removed resource, or {@code null} if the store did not contain a resource with the given name
	 */
	public Resource removeResource(String name) {
		synchronized (this) {
			Resource resource = getResource(name);
			removeResource(resource);
			return resource;
		}
	}

	public boolean removeResource(Resource resource) {
		synchronized (this) {
			return resources.remove(resource);
		}
	}
}