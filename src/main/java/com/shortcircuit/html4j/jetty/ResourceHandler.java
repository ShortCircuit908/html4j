package com.shortcircuit.html4j.jetty;

import com.shortcircuit.html4j.page.Resource;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * An implementation of Jetty's {@link org.eclipse.jetty.server.Handler} to manage and serve a virtual store of
 * {@link com.shortcircuit.html4j.page.Resource Resource}s<br>
 * <p>
 * Resources are accessed via the URL {@code base_url/resourcename}.<br>
 * The handler is lenient with trailing slashes, meaning both {@code base_url/resourcename} and {@code base_url/resourcename/}
 * work.<br>
 * Resources whose names contain the separator {@code /} are shown as being located within a new directory.<br>
 * A resource with the names {@code dir/resourcename} is shown as a resource with the name {@code resourcename} located
 * within the directory {@code dir}.
 * </p>
 * <p>
 * Servlet requests with no target specified ({@code /}) are redirected to the handler's "home page" resource.<br>
 * By default, this points to a resource called "index.html". However, the home page may be changed by calling
 * {@link #setHomePage(String)}.
 * </p>
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class ResourceHandler extends AbstractHandler {
	private final ArrayList<Resource> resources = new ArrayList<>();
	private String home_page = "index.html";

	@Override
	public void handle(String target, Request base_request, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if (target.equals("/")) {
			target = home_page;
		}
		Resource resource = getResource(target);
		if (resource == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			base_request.setHandled(false);
			return;
		}
		response.setContentType(resource.getContentType());
		base_request.setHandled(resource.writeResource(base_request, request, response));
		try {
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
		catch (Exception e) {
			// Do nothing
		}
	}

	/**
	 * Sets this handler's "home page"<br>
	 * <p>
	 * The home page is a resource which will be served in response to a request for the root target
	 * </p>
	 *
	 * @param home_page the name of the resource
	 */
	public void setHomePage(String home_page) {
		this.home_page = home_page;
	}

	/**
	 * Get a resource by its name<br>
	 * <p>
	 * This method ignores leading and trailing {@code /} characters
	 * </p>
	 *
	 * @param name the name of the resource
	 * @return the specified resource
	 */

	public Resource getResource(String name) {
		if (name == null) {
			return null;
		}
		name = name.substring(name.startsWith("/") ? 1 : 0, name.endsWith("/") ? name.length() - 1 : name.length());
		synchronized (this) {
			for (Resource resource : resources) {
				if (resource.getName().equals(name)) {
					return resource;
				}
			}
			return null;
		}
	}

	/**
	 * Add a resource to the store<br>
	 * <p>
	 * If the handler contains a resource with an identical name, the existing resource is replaced and returned.
	 * </p>
	 *
	 * @param resource the new resource to add
	 * @return the overwritten resource
	 */
	public Resource addResource(Resource resource) {
		synchronized (this) {
			Resource removed = removeResource(resource.getName());
			resources.add(resource);
			return removed;
		}
	}

	/**
	 * Remove a resource from the store<br>
	 *
	 * @param name the name of the resource
	 * @return the removed resource, or {@code null} if the store did not contain a resource with the given name
	 */
	public Resource removeResource(String name) {
		synchronized (this) {
			Resource resource = getResource(name);
			removeResource(resource);
			return resource;
		}
	}

	public boolean removeResource(Resource resource) {
		synchronized (this) {
			return resources.remove(resource);
		}
	}
}
