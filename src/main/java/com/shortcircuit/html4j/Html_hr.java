package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_hr extends HtmlWrapper {
	public Html_hr() {
		super("hr");
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}
}
