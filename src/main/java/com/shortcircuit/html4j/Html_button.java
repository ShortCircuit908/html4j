package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_button extends HtmlContainer<HtmlWrapper> {

	public Html_button() {
		super("button");
	}

	public static final class AUTOFOCUS extends HtmlAttribute {
		public AUTOFOCUS() {
			super("autofocus", null);
		}
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class FORMACTION extends HtmlAttribute {
		public FORMACTION(String value) {
			super("formaction", value);
		}
	}

	public static final class FORMENCTYPE extends HtmlAttribute {
		public FORMENCTYPE(_FormEncType value) {
			super("formenctype", value.enctype);
		}

		private FORMENCTYPE(String value) {
			super("formenctype", value);
		}
	}

	public static final class FORMMETHOD extends HtmlAttribute {
		public FORMMETHOD(_FormMethod value) {
			super("formmethod", value.method);
		}

		private FORMMETHOD(String value) {
			super("formmethod", value);
		}
	}

	public static final class FORMNOVALIDATE extends HtmlAttribute {
		public FORMNOVALIDATE() {
			super("formnovalidate", null);
		}
	}

	public static final class FORMTARGET extends HtmlAttribute {
		public FORMTARGET(_FormTarget value) {
			this(value.target);
		}

		public FORMTARGET(String value) {
			super("formtarget", value);
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(_Type value) {
			super("type", value.type);
		}

		private TYPE(String value) {
			super("type", value);
		}
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(String value) {
			super("value", value);
		}
	}

	public enum _FormTarget {
		BLANK("_blank"),
		PARENT("_parent"),
		SELF("_self"),
		TOP("_top");
		private final String target;

		_FormTarget(String target) {
			this.target = target;
		}
	}

	public enum _FormEncType {
		APPLICATION("application/x-www-form-urlencoded"),
		MULTIPART("multipart/form-data"),
		PLAIN_TEXT("text/plain");

		private final String enctype;

		_FormEncType(String enctype) {
			this.enctype = enctype;
		}
	}

	public enum _FormMethod {
		GET("get"),
		POST("post");

		private final String method;

		_FormMethod(String method) {
			this.method = method;
		}
	}

	public enum _Type {
		BUTTON("button"),
		RESET("reset"),
		SUBMIT("submit");

		private final String type;

		_Type(String type) {
			this.type = type;
		}
	}
}
