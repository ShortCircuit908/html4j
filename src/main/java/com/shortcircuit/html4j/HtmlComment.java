package com.shortcircuit.html4j;

/**
 * Represents a commented-out portion of an HTML document
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class HtmlComment<T extends HtmlWrapper> extends HtmlContainer<T> {

	/**
	 * Construct a new HTML comment
	 */
	public HtmlComment() {
		super("<!---->");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getOpeningTag() {
		return "<!--";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getClosingTag() {
		return "-->";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHtmlString() {
		synchronized (this) {
			StringBuilder builder = new StringBuilder("<!--");
			for (T child : children) {
				builder.append(child.toHtmlString());
			}
			return builder.append("-->").toString();
		}
	}
}
