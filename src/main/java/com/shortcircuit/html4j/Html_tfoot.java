package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_tfoot<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_tfoot() {
		super("tfoot");
	}
}
