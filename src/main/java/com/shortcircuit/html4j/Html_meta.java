package com.shortcircuit.html4j;

import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_meta extends HtmlWrapper {

	public Html_meta() {
		super("meta", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class CHARSET extends HtmlAttribute {
		public CHARSET(Charset value) {
			super("charset", value.name());
		}

		public CHARSET(String value) throws UnsupportedCharsetException {
			super("charset", Charset.forName(value).name());
		}
	}

	public static final class CONTENT extends HtmlAttribute {
		public CONTENT(String value) {
			super("content", value);
		}
	}

	public static final class HTTP_EQUIV extends HtmlAttribute {
		public HTTP_EQUIV(_HttpEquiv value) {
			super("http-equiv", value.httpequiv);
		}

		private HTTP_EQUIV(String value) {
			super("http-equiv", value);
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(_Name value) {
			super("name", value.name);
		}

		private NAME(String value) {
			super("name", value);
		}
	}

	public enum _HttpEquiv {
		CONTENT_TYPE("content-type"),
		DEFAULT_STYLE("default-style"),
		REFRESH("refresh");

		private final String httpequiv;

		_HttpEquiv(String httpequiv) {
			this.httpequiv = httpequiv;
		}
	}

	public enum _Name {
		APPLICATION_NAME("application-name"),
		AUTHOR("author"),
		DESCRIPTION("description"),
		GENERATOR("generator"),
		KEYWORDS("keywords");

		private final String name;

		_Name(String name) {
			this.name = name;
		}
	}
}
