package com.shortcircuit.html4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_del extends HtmlContainer<HtmlWrapper> {

	public Html_del() {
		super("del");
	}

	public static final class CITE extends HtmlAttribute {
		public CITE(String value) {
			super("cite", value);
		}
	}

	public static final class DATETIME extends HtmlAttribute {
		private static final SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DDThh:mm:ssTZD");

		public DATETIME(Date value) {
			super("datetime", format.format(value));
		}

		private DATETIME(String value) {
			super("datetime", value);
		}
	}
}
