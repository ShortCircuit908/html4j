package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_select extends HtmlContainer<HtmlWrapper> {

	public Html_select() {
		super("select");
	}

	public static final class AUTOFOCUS extends HtmlAttribute {
		public AUTOFOCUS() {
			super("autofocus", null);
		}
	}

	public static final class DISABLED extends HtmlAttribute {
		public DISABLED() {
			super("disabled", null);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}

	public static final class MULTIPLE extends HtmlAttribute {
		public MULTIPLE() {
			super("multiple", null);
		}
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class REQUIRED extends HtmlAttribute {
		public REQUIRED() {
			super("required", null);
		}
	}

	public static final class SIZE extends HtmlAttribute {
		public SIZE(Number value) {
			super("size", value == null ? "0" : value.toString());
		}
	}
}
