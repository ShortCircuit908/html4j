package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_strong<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_strong() {
		super("strong");
	}
}
