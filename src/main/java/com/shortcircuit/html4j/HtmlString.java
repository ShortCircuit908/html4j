package com.shortcircuit.html4j;

import org.apache.commons.lang3.StringEscapeUtils;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Represents a String for use in HTML elements
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
@SuppressWarnings("NullableProblems")
public class HtmlString extends HtmlWrapper implements List<Object> {
	private final ArrayList<Object> parts = new ArrayList<>();

	/**
	 * Construct a new HTML string
	 */
	public HtmlString() {
		super("<!---->", true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHtmlString() {
		synchronized (this) {
			StringBuilder builder = new StringBuilder();
			for (Object part : parts) {
				if (part == null) {
					builder.append("null");
					continue;
				}
				if (part instanceof HtmlWrapper) {
					builder.append(((HtmlWrapper) part).toHtmlString());
					continue;
				}
				builder.append(StringEscapeUtils.escapeHtml4(part.toString()));
			}
			return builder.toString();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HtmlString copy() {
		synchronized (this) {
			HtmlString copy = new HtmlString();
			for (HtmlAttribute attribute : attributes) {
				copy.setAttribute(attribute);
			}
			for (Object part : parts) {
				if (part instanceof HtmlContainer) {
					copy.add(((HtmlContainer) part).deepCopy());
				}
				else if (part instanceof HtmlWrapper) {
					copy.add(((HtmlWrapper) part).copy());
				}
				else {
					if (part instanceof Cloneable) {
						try {
							Method clone_method = part.getClass().getDeclaredMethod("clone");
							clone_method.setAccessible(true);
							copy.add(clone_method.invoke(part));
							continue;
						}
						catch (ReflectiveOperationException e) {
							// Do nothing
						}
					}
					copy.add(part);
				}
			}
			return copy;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return parts.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return parts.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object o) {
		return parts.contains(o);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<Object> iterator() {
		return parts.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toArray() {
		return parts.toArray();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T[] toArray(T[] a) {
		return parts.toArray(a);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean add(Object e) {
		synchronized (this) {
			return parts.add(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object o) {
		synchronized (this) {
			return parts.remove(o);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		return parts.containsAll(c);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(Collection<?> c) {
		synchronized (this) {
			return parts.addAll(c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(int index, Collection<?> c) {
		synchronized (this) {
			return parts.addAll(index, c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		synchronized (this) {
			return parts.removeAll(c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		synchronized (this) {
			return parts.retainAll(c);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		synchronized (this) {
			parts.clear();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object get(int index) {
		synchronized (this) {
			return parts.get(index);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object set(int index, Object element) {
		synchronized (this) {
			return parts.set(index, element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(int index, Object element) {
		synchronized (this) {
			parts.add(index, element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object remove(int index) {
		synchronized (this) {
			return parts.remove(index);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int indexOf(Object o) {
		return parts.indexOf(o);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int lastIndexOf(Object o) {
		return parts.lastIndexOf(o);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListIterator<Object> listIterator() {
		return parts.listIterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListIterator<Object> listIterator(int index) {
		return parts.listIterator(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> subList(int fromIndex, int toIndex) {
		synchronized (this) {
			return parts.subList(fromIndex, toIndex);
		}
	}
}
