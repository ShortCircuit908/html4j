package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_ol<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_ol() {
		super("ol");
	}

	public static final class REVERSED extends HtmlAttribute {
		public REVERSED() {
			super("reversed", null);
		}
	}

	public static final class START extends HtmlAttribute {
		public START(Number value) {
			super("start", value == null ? "0" : value.toString());
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(_Type value) {
			super("type", value.type);
		}

		private TYPE(String value) {
			super("type", value);
		}
	}

	public enum _Type {
		NUMERIC("1"),
		ALPHA_UPPER("A"),
		ALPHA_LOWER("a"),
		ROMAN_UPPER("I"),
		ROMAN_LOWER("i");

		private final String type;

		_Type(String type) {
			this.type = type;
		}
	}
}
