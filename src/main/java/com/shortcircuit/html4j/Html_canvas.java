package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_canvas<T extends HtmlWrapper> extends HtmlContainer<T> {
	public Html_canvas() {
		super("canvas");
	}

	public static final class HEIGHT extends HtmlAttribute {
		public HEIGHT(Number value) {
			super("height", value == null ? "0" : value.toString());
		}
	}

	public static final class WIDTH extends HtmlAttribute {
		public WIDTH(Number value) {
			super("width", value == null ? "0" : value.toString());
		}
	}
}
