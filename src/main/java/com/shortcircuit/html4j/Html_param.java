package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_param extends HtmlWrapper {

	public Html_param() {
		super("param", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class NAME extends HtmlAttribute {
		public NAME(String value) {
			super("name", value);
		}
	}

	public static final class VALUE extends HtmlAttribute {
		public VALUE(String value) {
			super("value", value);
		}
	}
}
