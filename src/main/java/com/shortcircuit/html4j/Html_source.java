package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_source extends HtmlContainer<HtmlWrapper> {

	public Html_source() {
		super("source");
	}

	public static final class MEDIA extends HtmlAttribute {
		public MEDIA(String value) {
			super("media", value);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class TYPE extends HtmlAttribute {
		public TYPE(String value) {
			super("type", value);
		}
	}
}
