package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_img extends HtmlWrapper {
	public Html_img() {
		super("img", true);
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag();
	}

	public static final class ALT extends HtmlAttribute {
		public ALT(String value) {
			super("alt", value);
		}
	}

	public static final class CROSSORIGIN extends HtmlAttribute {
		public CROSSORIGIN(_Crossorigin value) {
			super("crossorigin", value.crossorigin);
		}

		private CROSSORIGIN(String value){
			super("crossorigin", value);
		}
	}

	public static final class HEIGHT extends HtmlAttribute {
		public HEIGHT(Number value) {
			super("height", value == null ? "0" : value.toString());
		}
	}

	public static final class ISMAP extends HtmlAttribute {
		public ISMAP() {
			super("ismap", null);
		}
	}

	public static final class LONGDESC extends HtmlAttribute {
		public LONGDESC(String value) {
			super("longdesc", value);
		}
	}

	public static final class SRC extends HtmlAttribute {
		public SRC(String value) {
			super("src", value);
		}
	}

	public static final class USEMAP extends HtmlAttribute {
		public USEMAP(String value) {
			super("src", value);
		}
	}

	public static final class WIDTH extends HtmlAttribute {
		public WIDTH(Number value) {
			super("width", value == null ? "0" : value.toString());
		}
	}

	public enum _Crossorigin {
		ANONYMOUS("anonymous"),
		USE_CREDENTIALS("use-credentials");

		private final String crossorigin;

		_Crossorigin(String crossorigin) {
			this.crossorigin = crossorigin;
		}
	}
}
