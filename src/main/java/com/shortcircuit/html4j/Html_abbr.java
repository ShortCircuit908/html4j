package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_abbr extends HtmlWrapper {
	private HtmlWrapper value;

	public Html_abbr() {
		this(new HtmlString());
	}

	public Html_abbr(HtmlWrapper value) {
		super("abbr");
		setValue(value);
	}

	public HtmlWrapper getValue() {
		return value;
	}

	public Html_abbr setValue(HtmlWrapper value) {
		this.value = value;
		return this;
	}

	@Override
	public String toHtmlString() {
		return getOpeningTag() + (value == null ? "" : value.toHtmlString()) + getClosingTag();
	}
}
