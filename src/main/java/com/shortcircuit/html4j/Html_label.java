package com.shortcircuit.html4j;

/**
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class Html_label extends HtmlContainer<HtmlWrapper> {

	public Html_label() {
		super("label");
	}

	public static final class FOR extends HtmlAttribute {
		public FOR(String value) {
			super("for", value);
		}
	}

	public static final class FORM extends HtmlAttribute {
		public FORM(String value) {
			super("form", value);
		}
	}
}
